job ('my-awesome-project')
{
    scm {
        git('https://gitlab.com/jedrzwi1/final-awesome-project.git') { node ->
            node / gitConfigName('Python guy')
            node / gitConfigEmail('jenkins@jenkins-dsl.com')
        }
    }

    triggers {
        scm('H/5 * * * *')
    }

    steps {
        python {
            command('python Tests.py')
    }
        //sh('python Tests.py')
    }

}